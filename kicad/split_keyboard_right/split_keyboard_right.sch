EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	750  1000 1000 1000
Wire Wire Line
	750  3000 1000 3000
$Comp
L Device:R R1
U 1 1 60AA59B1
P 1000 2000
F 0 "R1" H 1070 2046 50  0000 L CNN
F 1 "100k" H 1070 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 930 2000 50  0001 C CNN
F 3 "~" H 1000 2000 50  0001 C CNN
	1    1000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1000 1000 1850
Connection ~ 1000 1000
Wire Wire Line
	1000 1000 1500 1000
Wire Wire Line
	1000 2150 1000 3000
Connection ~ 1000 3000
Wire Wire Line
	1000 3000 1500 3000
$Comp
L Device:C C1
U 1 1 60AA5DC0
P 1500 2000
F 0 "C1" H 1615 2046 50  0000 L CNN
F 1 "4.7u" H 1615 1955 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 1538 1850 50  0001 C CNN
F 3 "~" H 1500 2000 50  0001 C CNN
	1    1500 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1850 1500 1000
Connection ~ 1500 1000
Wire Wire Line
	1500 2150 1500 3000
Connection ~ 1500 3000
Wire Wire Line
	1500 3000 2000 3000
$Comp
L Device:R R2
U 1 1 60AA6B57
P 2000 2500
F 0 "R2" H 2070 2546 50  0000 L CNN
F 1 "2k" H 2070 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1930 2500 50  0001 C CNN
F 3 "~" H 2000 2500 50  0001 C CNN
	1    2000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2350 2000 2000
Wire Wire Line
	2000 2650 2000 3000
Connection ~ 2000 3000
Wire Wire Line
	2000 3000 2500 3000
Wire Wire Line
	2500 2000 2000 2000
Wire Wire Line
	2500 2200 2500 3000
Connection ~ 2500 3000
$Comp
L Device:C C2
U 1 1 60AA7ADD
P 4000 2500
F 0 "C2" H 4115 2546 50  0000 L CNN
F 1 "4.7u" H 4115 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4038 2350 50  0001 C CNN
F 3 "~" H 4000 2500 50  0001 C CNN
	1    4000 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 60AA808F
P 4550 2450
F 0 "J1" H 4658 2631 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4658 2540 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 4550 2450 50  0001 C CNN
F 3 "~" H 4550 2450 50  0001 C CNN
	1    4550 2450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRLML6402 Q1
U 1 1 60AA8B9D
P 5500 1900
F 0 "Q1" V 5749 1900 50  0000 C CNN
F 1 "IRLML6402" V 5840 1900 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5700 1825 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/irlml6402pbf.pdf?fileId=5546d462533600a401535668d5c2263c" H 5500 1900 50  0001 L CNN
	1    5500 1900
	0    -1   1    0   
$EndComp
Wire Wire Line
	2500 3000 4000 3000
Wire Wire Line
	5500 1000 5500 1700
Wire Wire Line
	5300 2000 4750 2000
Wire Wire Line
	4000 2350 4000 2000
Connection ~ 4000 2000
Wire Wire Line
	4000 2000 3900 2000
Wire Wire Line
	4750 2450 4750 2000
Connection ~ 4750 2000
Wire Wire Line
	4750 2000 4000 2000
Wire Wire Line
	4000 2650 4000 3000
Connection ~ 4000 3000
Wire Wire Line
	4000 3000 4750 3000
Wire Wire Line
	4750 2550 4750 3000
Connection ~ 4750 3000
Wire Wire Line
	4750 3000 7000 3000
$Comp
L Device:LED D1
U 1 1 60AAE601
P 4500 1250
F 0 "D1" V 4539 1132 50  0000 R CNN
F 1 "LED" V 4448 1132 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 4500 1250 50  0001 C CNN
F 3 "~" H 4500 1250 50  0001 C CNN
	1    4500 1250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 60AAF0A8
P 4500 1650
F 0 "R3" H 4570 1696 50  0000 L CNN
F 1 "470" H 4570 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4430 1650 50  0001 C CNN
F 3 "~" H 4500 1650 50  0001 C CNN
	1    4500 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1800 3900 1800
Wire Wire Line
	4500 1500 4500 1400
Wire Wire Line
	4500 1100 4500 1000
Connection ~ 4500 1000
Wire Wire Line
	4500 1000 5500 1000
$Comp
L Device:C C3
U 1 1 60AB3FAD
P 7500 2700
F 0 "C3" H 7615 2746 50  0000 L CNN
F 1 "1u" H 7615 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7538 2550 50  0001 C CNN
F 3 "~" H 7500 2700 50  0001 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2600 7000 3000
Connection ~ 7000 3000
Wire Wire Line
	7000 3000 7500 3000
Wire Wire Line
	7000 2100 7000 2400
Wire Wire Line
	7000 2500 7500 2500
Wire Wire Line
	7500 2500 7500 2550
Wire Wire Line
	7500 2850 7500 3000
Wire Wire Line
	5700 2000 6600 2000
Wire Wire Line
	7500 2500 7750 2500
Connection ~ 7500 2500
Wire Wire Line
	5800 2600 5500 2600
Text GLabel 5500 2600 0    50   Input ~ 0
Vsys
Text GLabel 7750 2500 2    50   Input ~ 0
Vsys
Text GLabel 9300 1100 0    50   Input ~ 0
Vsys
Text GLabel 750  1000 0    50   Input ~ 0
Vin
Text GLabel 9300 2900 0    50   Input ~ 0
Vin
$Comp
L power:GND #PWR01
U 1 1 60ABBC5A
P 750 3000
F 0 "#PWR01" H 750 2750 50  0001 C CNN
F 1 "GND" H 755 2827 50  0000 C CNN
F 2 "" H 750 3000 50  0001 C CNN
F 3 "" H 750 3000 50  0001 C CNN
	1    750  3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 60ABCF15
P 8650 2400
F 0 "#PWR01" H 8650 2150 50  0001 C CNN
F 1 "GND" H 8655 2227 50  0000 C CNN
F 2 "" H 8650 2400 50  0001 C CNN
F 3 "" H 8650 2400 50  0001 C CNN
	1    8650 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 5500 11300 5500
Wire Wire Line
	11700 5500 12000 5500
Text GLabel 12000 5500 2    50   Input ~ 0
SW_setting
$Comp
L Device:R R5
U 1 1 60AD9011
P 12000 4000
F 0 "R5" V 11793 4000 50  0000 C CNN
F 1 "10" V 11884 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11930 4000 50  0001 C CNN
F 3 "~" H 12000 4000 50  0001 C CNN
	1    12000 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 60AD9686
P 12000 3500
F 0 "R4" V 11793 3500 50  0000 C CNN
F 1 "10" V 11884 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11930 3500 50  0001 C CNN
F 3 "~" H 12000 3500 50  0001 C CNN
	1    12000 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60AD9B26
P 12000 4500
F 0 "R6" V 11793 4500 50  0000 C CNN
F 1 "10" V 11884 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11930 4500 50  0001 C CNN
F 3 "~" H 12000 4500 50  0001 C CNN
	1    12000 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 60AD9DC9
P 12500 3500
F 0 "R7" V 12293 3500 50  0000 C CNN
F 1 "47" V 12384 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 12430 3500 50  0001 C CNN
F 3 "~" H 12500 3500 50  0001 C CNN
	1    12500 3500
	0    1    1    0   
$EndComp
Text GLabel 12750 3500 2    50   Input ~ 0
LED_R
Text GLabel 12750 4000 2    50   Input ~ 0
LED_G
Text GLabel 12750 4500 2    50   Input ~ 0
LED_B
$Comp
L power:GND #PWR01
U 1 1 60AE542B
P 11000 4500
F 0 "#PWR01" H 11000 4250 50  0001 C CNN
F 1 "GND" H 11005 4327 50  0000 C CNN
F 2 "" H 11000 4500 50  0001 C CNN
F 3 "" H 11000 4500 50  0001 C CNN
	1    11000 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_RCBG D2
U 1 1 60AE5DD1
P 11500 4000
F 0 "D2" H 11500 4497 50  0000 C CNN
F 1 "LED_RCBG" H 11500 4406 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB" H 11500 3950 50  0001 C CNN
F 3 "~" H 11500 3950 50  0001 C CNN
	1    11500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	12750 3500 12650 3500
Wire Wire Line
	12350 3500 12150 3500
Wire Wire Line
	11850 3500 11750 3500
Wire Wire Line
	11750 3500 11750 3800
Wire Wire Line
	11750 3800 11700 3800
Wire Wire Line
	11850 4000 11700 4000
Wire Wire Line
	11850 4500 11750 4500
Wire Wire Line
	11750 4500 11750 4200
Wire Wire Line
	11750 4200 11700 4200
Wire Wire Line
	12150 4000 12750 4000
Wire Wire Line
	12150 4500 12750 4500
Wire Wire Line
	11300 4000 11000 4000
Wire Wire Line
	11000 4000 11000 4500
Text GLabel 9300 2100 0    50   Input ~ 0
LED_R
Text GLabel 9300 1900 0    50   Input ~ 0
LED_G
Text GLabel 9300 2000 0    50   Input ~ 0
LED_B
NoConn ~ 7000 1900
$Comp
L Switch:SW_Push_Dual_x2 SW2
U 1 1 6136E563
P 11500 5500
F 0 "SW2" H 11500 5785 50  0000 C CNN
F 1 "SW_Push_Dual_x2" H 11500 5694 50  0000 C CNN
F 2 "Ble_keyboard:SW_Push_Dual_x2_TS-4401" H 11500 5700 50  0001 C CNN
F 3 "~" H 11500 5700 50  0001 C CNN
	1    11500 5500
	1    0    0    -1  
$EndComp
$Comp
L Ble_Keyboard:ADP3338AKCZ-3.3-RL IC1
U 1 1 613770E8
P 7000 2600
F 0 "IC1" H 7600 2135 50  0000 C CNN
F 1 "ADP3338AKCZ-3.3-RL" H 7600 2226 50  0000 C CNN
F 2 "Ble_keyboard:SOT230P700X180-4N" H 8050 2700 50  0001 L CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/ADP3338.pdf" H 8050 2600 50  0001 L CNN
F 4 "LDO Voltage Regulator anyCAP 1A SOT223-3 Analog Devices ADP3338AKCZ-3.3-RL, LDO Voltage Regulator, 1.6A, 3.3 V +/-0.8%, 2.7  8 Vin, 3 + Tab-Pin SOT-223" H 8050 2500 50  0001 L CNN "Description"
F 5 "1.8" H 8050 2400 50  0001 L CNN "Height"
F 6 "584-ADP3338AKCZ3.3R" H 8050 2300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADP3338AKCZ-33-RL?qs=WIvQP4zGangPtIg4n%252B1tTA%3D%3D" H 8050 2200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Analog Devices" H 8050 2100 50  0001 L CNN "Manufacturer_Name"
F 9 "ADP3338AKCZ-3.3-RL" H 8050 2000 50  0001 L CNN "Manufacturer_Part_Number"
	1    7000 2600
	-1   0    0    1   
$EndComp
$Comp
L Evaluation_board:ESP32-DEVKITC-32D U2
U 1 1 6138E68F
P 10100 2000
F 0 "U2" H 10100 3167 50  0000 C CNN
F 1 "ESP32-DEVKITC-32D" H 10100 3076 50  0000 C CNN
F 2 "Evaluation_board:MODULE_ESP32-DEVKITC-32D" H 10100 2000 50  0001 L BNN
F 3 "" H 10100 2000 50  0001 L BNN
F 4 "Espressif Systems" H 10100 2000 50  0001 L BNN "MANUFACTURER"
F 5 "4" H 10100 2000 50  0001 L BNN "PARTREV"
	1    10100 2000
	1    0    0    -1  
$EndComp
Text GLabel 800  6000 3    50   Input ~ 0
SW_Key_In_1
Text GLabel 9700 3500 2    50   Input ~ 0
SW_Key_Out_1
Text GLabel 9700 4000 2    50   Input ~ 0
SW_Key_Out_2
Text GLabel 9700 4500 2    50   Input ~ 0
SW_Key_Out_3
Text GLabel 9700 5000 2    50   Input ~ 0
SW_Key_Out_4
Text GLabel 9700 5500 2    50   Input ~ 0
SW_Key_Out_5
$Comp
L Device:D D38
U 1 1 613E1F85
P 8950 3900
F 0 "D38" H 8950 4117 50  0000 C CNN
F 1 "D" H 8950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 8950 3900 50  0001 C CNN
F 3 "~" H 8950 3900 50  0001 C CNN
	1    8950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW38
U 1 1 613E4305
P 9400 3900
F 0 "SW38" H 9400 4185 50  0000 C CNN
F 1 "SW_Push" H 9400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 9400 4100 50  0001 C CNN
F 3 "~" H 9400 4100 50  0001 C CNN
	1    9400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3900 9100 3900
Wire Wire Line
	9600 3900 9600 3500
Wire Wire Line
	8800 3900 8800 4400
$Comp
L Device:D D39
U 1 1 613ED446
P 8950 4400
F 0 "D39" H 8950 4617 50  0000 C CNN
F 1 "D" H 8950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 8950 4400 50  0001 C CNN
F 3 "~" H 8950 4400 50  0001 C CNN
	1    8950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW39
U 1 1 613ED450
P 9400 4400
F 0 "SW39" H 9400 4685 50  0000 C CNN
F 1 "SW_Push" H 9400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 9400 4600 50  0001 C CNN
F 3 "~" H 9400 4600 50  0001 C CNN
	1    9400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4400 9100 4400
Wire Wire Line
	9600 4400 9600 4000
Connection ~ 8800 4400
$Comp
L Device:D D37
U 1 1 61404B61
P 7950 4400
F 0 "D37" H 7950 4617 50  0000 C CNN
F 1 "D" H 7950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 7950 4400 50  0001 C CNN
F 3 "~" H 7950 4400 50  0001 C CNN
	1    7950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW37
U 1 1 61404B67
P 8400 4400
F 0 "SW37" H 8400 4685 50  0000 C CNN
F 1 "SW_Push" H 8400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 8400 4600 50  0001 C CNN
F 3 "~" H 8400 4600 50  0001 C CNN
	1    8400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4400 8100 4400
$Comp
L Device:D D36
U 1 1 61406BFD
P 7950 3900
F 0 "D36" H 7950 4117 50  0000 C CNN
F 1 "D" H 7950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 7950 3900 50  0001 C CNN
F 3 "~" H 7950 3900 50  0001 C CNN
	1    7950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW36
U 1 1 61406C03
P 8400 3900
F 0 "SW36" H 8400 4185 50  0000 C CNN
F 1 "SW_Push" H 8400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 8400 4100 50  0001 C CNN
F 3 "~" H 8400 4100 50  0001 C CNN
	1    8400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3900 8100 3900
Wire Wire Line
	8600 3900 8600 3500
$Comp
L Device:D D9
U 1 1 61408C23
P 1950 4900
F 0 "D9" H 1950 5117 50  0000 C CNN
F 1 "D" H 1950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 1950 4900 50  0001 C CNN
F 3 "~" H 1950 4900 50  0001 C CNN
	1    1950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 61408C29
P 2400 4900
F 0 "SW9" H 2400 5185 50  0000 C CNN
F 1 "SW_Push" H 2400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 2400 5100 50  0001 C CNN
F 3 "~" H 2400 5100 50  0001 C CNN
	1    2400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4900 2100 4900
Wire Wire Line
	2600 4900 2600 4500
$Comp
L Device:D D34
U 1 1 6140A97B
P 6950 5400
F 0 "D34" H 6950 5617 50  0000 C CNN
F 1 "D" H 6950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 6950 5400 50  0001 C CNN
F 3 "~" H 6950 5400 50  0001 C CNN
	1    6950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW34
U 1 1 6140A981
P 7400 5400
F 0 "SW34" H 7400 5685 50  0000 C CNN
F 1 "SW_Push" H 7400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 7400 5600 50  0001 C CNN
F 3 "~" H 7400 5600 50  0001 C CNN
	1    7400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5400 7100 5400
Wire Wire Line
	7600 5400 7600 5000
Wire Wire Line
	7800 3900 7800 4400
Connection ~ 7800 4400
Text GLabel 1800 6000 3    50   Input ~ 0
SW_Key_In_2
$Comp
L Device:D D32
U 1 1 61427905
P 6950 4400
F 0 "D32" H 6950 4617 50  0000 C CNN
F 1 "D" H 6950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 6950 4400 50  0001 C CNN
F 3 "~" H 6950 4400 50  0001 C CNN
	1    6950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW32
U 1 1 6142790B
P 7400 4400
F 0 "SW32" H 7400 4685 50  0000 C CNN
F 1 "SW_Push" H 7400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 7400 4600 50  0001 C CNN
F 3 "~" H 7400 4600 50  0001 C CNN
	1    7400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4400 7100 4400
Wire Wire Line
	7600 4400 7600 4000
$Comp
L Device:D D31
U 1 1 61427913
P 6950 3900
F 0 "D31" H 6950 4117 50  0000 C CNN
F 1 "D" H 6950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 6950 3900 50  0001 C CNN
F 3 "~" H 6950 3900 50  0001 C CNN
	1    6950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW31
U 1 1 61427919
P 7400 3900
F 0 "SW31" H 7400 4185 50  0000 C CNN
F 1 "SW_Push" H 7400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 7400 4100 50  0001 C CNN
F 3 "~" H 7400 4100 50  0001 C CNN
	1    7400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3900 7100 3900
Wire Wire Line
	7600 3900 7600 3500
$Comp
L Device:D D33
U 1 1 61427921
P 6950 4900
F 0 "D33" H 6950 5117 50  0000 C CNN
F 1 "D" H 6950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 6950 4900 50  0001 C CNN
F 3 "~" H 6950 4900 50  0001 C CNN
	1    6950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW33
U 1 1 61427927
P 7400 4900
F 0 "SW33" H 7400 5185 50  0000 C CNN
F 1 "SW_Push" H 7400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 7400 5100 50  0001 C CNN
F 3 "~" H 7400 5100 50  0001 C CNN
	1    7400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4900 7100 4900
Wire Wire Line
	7600 4900 7600 4500
$Comp
L Device:D D15
U 1 1 6142792F
P 2950 5400
F 0 "D15" H 2950 5617 50  0000 C CNN
F 1 "D" H 2950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 2950 5400 50  0001 C CNN
F 3 "~" H 2950 5400 50  0001 C CNN
	1    2950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW15
U 1 1 61427935
P 3400 5400
F 0 "SW15" H 3400 5685 50  0000 C CNN
F 1 "SW_Push" H 3400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 3400 5600 50  0001 C CNN
F 3 "~" H 3400 5600 50  0001 C CNN
	1    3400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 5400 3100 5400
Wire Wire Line
	3600 5400 3600 5000
Wire Wire Line
	6800 3900 6800 4400
Connection ~ 6800 4400
Text GLabel 2800 6000 3    50   Input ~ 0
SW_Key_In_3
Text GLabel 3800 6000 3    50   Input ~ 0
SW_Key_In_4
$Comp
L Device:D D26
U 1 1 614E4AFC
P 5950 3900
F 0 "D26" H 5950 4117 50  0000 C CNN
F 1 "D" H 5950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 5950 3900 50  0001 C CNN
F 3 "~" H 5950 3900 50  0001 C CNN
	1    5950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW26
U 1 1 614E4B02
P 6400 3900
F 0 "SW26" H 6400 4185 50  0000 C CNN
F 1 "SW_Push" H 6400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 6400 4100 50  0001 C CNN
F 3 "~" H 6400 4100 50  0001 C CNN
	1    6400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3900 6100 3900
Wire Wire Line
	6600 3900 6600 3500
Wire Wire Line
	5800 3900 5800 4400
$Comp
L Device:D D27
U 1 1 614E4B0B
P 5950 4400
F 0 "D27" H 5950 4617 50  0000 C CNN
F 1 "D" H 5950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 5950 4400 50  0001 C CNN
F 3 "~" H 5950 4400 50  0001 C CNN
	1    5950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW27
U 1 1 614E4B11
P 6400 4400
F 0 "SW27" H 6400 4685 50  0000 C CNN
F 1 "SW_Push" H 6400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 6400 4600 50  0001 C CNN
F 3 "~" H 6400 4600 50  0001 C CNN
	1    6400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4400 6100 4400
Wire Wire Line
	6600 4400 6600 4000
$Comp
L Device:D D29
U 1 1 614E4B19
P 5950 5400
F 0 "D29" H 5950 5617 50  0000 C CNN
F 1 "D" H 5950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 5950 5400 50  0001 C CNN
F 3 "~" H 5950 5400 50  0001 C CNN
	1    5950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW29
U 1 1 614E4B1F
P 6400 5400
F 0 "SW29" H 6400 5685 50  0000 C CNN
F 1 "SW_Push" H 6400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 6400 5600 50  0001 C CNN
F 3 "~" H 6400 5600 50  0001 C CNN
	1    6400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5400 6100 5400
Wire Wire Line
	6600 5400 6600 5000
$Comp
L Device:D D35
U 1 1 614E4B27
P 6950 5900
F 0 "D35" H 6950 6117 50  0000 C CNN
F 1 "D" H 6950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 6950 5900 50  0001 C CNN
F 3 "~" H 6950 5900 50  0001 C CNN
	1    6950 5900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW35
U 1 1 614E4B2D
P 7400 5900
F 0 "SW35" H 7400 6185 50  0000 C CNN
F 1 "SW_Push" H 7400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 7400 6100 50  0001 C CNN
F 3 "~" H 7400 6100 50  0001 C CNN
	1    7400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5900 7100 5900
Wire Wire Line
	7600 5900 7600 5500
$Comp
L Device:D D28
U 1 1 614E4B38
P 5950 4900
F 0 "D28" H 5950 5117 50  0000 C CNN
F 1 "D" H 5950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 5950 4900 50  0001 C CNN
F 3 "~" H 5950 4900 50  0001 C CNN
	1    5950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW28
U 1 1 614E4B3E
P 6400 4900
F 0 "SW28" H 6400 5185 50  0000 C CNN
F 1 "SW_Push" H 6400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 6400 5100 50  0001 C CNN
F 3 "~" H 6400 5100 50  0001 C CNN
	1    6400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4900 6100 4900
Wire Wire Line
	6600 4900 6600 4500
Wire Wire Line
	5800 4400 5800 4900
Connection ~ 5800 4400
Wire Wire Line
	5800 5400 5800 4900
Connection ~ 5800 5400
Connection ~ 5800 4900
$Comp
L Device:D D22
U 1 1 614E4B4B
P 4950 4400
F 0 "D22" H 4950 4617 50  0000 C CNN
F 1 "D" H 4950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 4950 4400 50  0001 C CNN
F 3 "~" H 4950 4400 50  0001 C CNN
	1    4950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW22
U 1 1 614E4B51
P 5400 4400
F 0 "SW22" H 5400 4685 50  0000 C CNN
F 1 "SW_Push" H 5400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 5400 4600 50  0001 C CNN
F 3 "~" H 5400 4600 50  0001 C CNN
	1    5400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4400 5100 4400
Wire Wire Line
	5600 4400 5600 4000
$Comp
L Device:D D21
U 1 1 614E4B59
P 4950 3900
F 0 "D21" H 4950 4117 50  0000 C CNN
F 1 "D" H 4950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 4950 3900 50  0001 C CNN
F 3 "~" H 4950 3900 50  0001 C CNN
	1    4950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW21
U 1 1 614E4B5F
P 5400 3900
F 0 "SW21" H 5400 4185 50  0000 C CNN
F 1 "SW_Push" H 5400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 5400 4100 50  0001 C CNN
F 3 "~" H 5400 4100 50  0001 C CNN
	1    5400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3900 5100 3900
Wire Wire Line
	5600 3900 5600 3500
$Comp
L Device:D D23
U 1 1 614E4B67
P 4950 4900
F 0 "D23" H 4950 5117 50  0000 C CNN
F 1 "D" H 4950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 4950 4900 50  0001 C CNN
F 3 "~" H 4950 4900 50  0001 C CNN
	1    4950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW23
U 1 1 614E4B6D
P 5400 4900
F 0 "SW23" H 5400 5185 50  0000 C CNN
F 1 "SW_Push" H 5400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 5400 5100 50  0001 C CNN
F 3 "~" H 5400 5100 50  0001 C CNN
	1    5400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4900 5100 4900
Wire Wire Line
	5600 4900 5600 4500
$Comp
L Device:D D24
U 1 1 614E4B75
P 4950 5400
F 0 "D24" H 4950 5617 50  0000 C CNN
F 1 "D" H 4950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 4950 5400 50  0001 C CNN
F 3 "~" H 4950 5400 50  0001 C CNN
	1    4950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW24
U 1 1 614E4B7B
P 5400 5400
F 0 "SW24" H 5400 5685 50  0000 C CNN
F 1 "SW_Push" H 5400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 5400 5600 50  0001 C CNN
F 3 "~" H 5400 5600 50  0001 C CNN
	1    5400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5400 5100 5400
Wire Wire Line
	5600 5400 5600 5000
$Comp
L Device:D D30
U 1 1 614E4B83
P 5950 5900
F 0 "D30" H 5950 6117 50  0000 C CNN
F 1 "D" H 5950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 5950 5900 50  0001 C CNN
F 3 "~" H 5950 5900 50  0001 C CNN
	1    5950 5900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW30
U 1 1 614E4B89
P 6400 5900
F 0 "SW30" H 6400 6185 50  0000 C CNN
F 1 "SW_Push" H 6400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 6400 6100 50  0001 C CNN
F 3 "~" H 6400 6100 50  0001 C CNN
	1    6400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5900 6100 5900
Wire Wire Line
	6600 5900 6600 5500
Wire Wire Line
	4800 3900 4800 4400
Connection ~ 4800 4400
Wire Wire Line
	4800 5400 4800 4900
Connection ~ 4800 5400
Text GLabel 4800 6000 3    50   Input ~ 0
SW_Key_In_5
$Comp
L Device:D D17
U 1 1 614E4B9B
P 3950 4400
F 0 "D17" H 3950 4617 50  0000 C CNN
F 1 "D" H 3950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 3950 4400 50  0001 C CNN
F 3 "~" H 3950 4400 50  0001 C CNN
	1    3950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW17
U 1 1 614E4BA1
P 4400 4400
F 0 "SW17" H 4400 4685 50  0000 C CNN
F 1 "SW_Push" H 4400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 4400 4600 50  0001 C CNN
F 3 "~" H 4400 4600 50  0001 C CNN
	1    4400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4400 4100 4400
Wire Wire Line
	4600 4400 4600 4000
$Comp
L Device:D D16
U 1 1 614E4BA9
P 3950 3900
F 0 "D16" H 3950 4117 50  0000 C CNN
F 1 "D" H 3950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 3950 3900 50  0001 C CNN
F 3 "~" H 3950 3900 50  0001 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 614E4BAF
P 4400 3900
F 0 "SW16" H 4400 4185 50  0000 C CNN
F 1 "SW_Push" H 4400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 4400 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3900 4100 3900
Wire Wire Line
	4600 3900 4600 3500
$Comp
L Device:D D18
U 1 1 614E4BB7
P 3950 4900
F 0 "D18" H 3950 5117 50  0000 C CNN
F 1 "D" H 3950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 3950 4900 50  0001 C CNN
F 3 "~" H 3950 4900 50  0001 C CNN
	1    3950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW18
U 1 1 614E4BBD
P 4400 4900
F 0 "SW18" H 4400 5185 50  0000 C CNN
F 1 "SW_Push" H 4400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 4400 5100 50  0001 C CNN
F 3 "~" H 4400 5100 50  0001 C CNN
	1    4400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4900 4100 4900
Wire Wire Line
	4600 4900 4600 4500
$Comp
L Device:D D19
U 1 1 614E4BC5
P 3950 5400
F 0 "D19" H 3950 5617 50  0000 C CNN
F 1 "D" H 3950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 3950 5400 50  0001 C CNN
F 3 "~" H 3950 5400 50  0001 C CNN
	1    3950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW19
U 1 1 614E4BCB
P 4400 5400
F 0 "SW19" H 4400 5685 50  0000 C CNN
F 1 "SW_Push" H 4400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 4400 5600 50  0001 C CNN
F 3 "~" H 4400 5600 50  0001 C CNN
	1    4400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5400 4100 5400
Wire Wire Line
	4600 5400 4600 5000
Wire Wire Line
	3800 3900 3800 4400
Connection ~ 3800 4400
Wire Wire Line
	3800 5400 3800 4900
Connection ~ 3800 5400
Text GLabel 5800 6000 3    50   Input ~ 0
SW_Key_In_6
Text GLabel 6800 6000 3    50   Input ~ 0
SW_Key_In_7
$Comp
L Device:D D12
U 1 1 6155A86D
P 2950 3900
F 0 "D12" H 2950 4117 50  0000 C CNN
F 1 "D" H 2950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 2950 3900 50  0001 C CNN
F 3 "~" H 2950 3900 50  0001 C CNN
	1    2950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 6155A873
P 3400 3900
F 0 "SW12" H 3400 4185 50  0000 C CNN
F 1 "SW_Push" H 3400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 3400 4100 50  0001 C CNN
F 3 "~" H 3400 4100 50  0001 C CNN
	1    3400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3900 3100 3900
Wire Wire Line
	3600 3900 3600 3500
Wire Wire Line
	2800 3900 2800 4400
$Comp
L Device:D D13
U 1 1 6155A87C
P 2950 4400
F 0 "D13" H 2950 4617 50  0000 C CNN
F 1 "D" H 2950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 2950 4400 50  0001 C CNN
F 3 "~" H 2950 4400 50  0001 C CNN
	1    2950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 6155A882
P 3400 4400
F 0 "SW13" H 3400 4685 50  0000 C CNN
F 1 "SW_Push" H 3400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 3400 4600 50  0001 C CNN
F 3 "~" H 3400 4600 50  0001 C CNN
	1    3400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4400 3100 4400
Wire Wire Line
	3600 4400 3600 4000
$Comp
L Device:D D41
U 1 1 6155A88A
P 8950 5400
F 0 "D41" H 8950 5617 50  0000 C CNN
F 1 "D" H 8950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 8950 5400 50  0001 C CNN
F 3 "~" H 8950 5400 50  0001 C CNN
	1    8950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW41
U 1 1 6155A890
P 9400 5400
F 0 "SW41" H 9400 5685 50  0000 C CNN
F 1 "SW_Push" H 9400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 9400 5600 50  0001 C CNN
F 3 "~" H 9400 5600 50  0001 C CNN
	1    9400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 5400 9100 5400
Wire Wire Line
	9600 5400 9600 5000
$Comp
L Device:D D25
U 1 1 6155A898
P 4950 5900
F 0 "D25" H 4950 6117 50  0000 C CNN
F 1 "D" H 4950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 4950 5900 50  0001 C CNN
F 3 "~" H 4950 5900 50  0001 C CNN
	1    4950 5900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW25
U 1 1 6155A89E
P 5400 5900
F 0 "SW25" H 5400 6185 50  0000 C CNN
F 1 "SW_Push" H 5400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 5400 6100 50  0001 C CNN
F 3 "~" H 5400 6100 50  0001 C CNN
	1    5400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5900 5100 5900
Wire Wire Line
	5600 5900 5600 5500
$Comp
L Device:D D14
U 1 1 6155A8A9
P 2950 4900
F 0 "D14" H 2950 5117 50  0000 C CNN
F 1 "D" H 2950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 2950 4900 50  0001 C CNN
F 3 "~" H 2950 4900 50  0001 C CNN
	1    2950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW14
U 1 1 6155A8AF
P 3400 4900
F 0 "SW14" H 3400 5185 50  0000 C CNN
F 1 "SW_Push" H 3400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 3400 5100 50  0001 C CNN
F 3 "~" H 3400 5100 50  0001 C CNN
	1    3400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4900 3100 4900
Wire Wire Line
	3600 4900 3600 4500
Wire Wire Line
	2800 4400 2800 4900
Connection ~ 2800 4400
Connection ~ 2800 4900
$Comp
L Device:D D8
U 1 1 6155A8BC
P 1950 4400
F 0 "D8" H 1950 4617 50  0000 C CNN
F 1 "D" H 1950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 1950 4400 50  0001 C CNN
F 3 "~" H 1950 4400 50  0001 C CNN
	1    1950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 6155A8C2
P 2400 4400
F 0 "SW8" H 2400 4685 50  0000 C CNN
F 1 "SW_Push" H 2400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 2400 4600 50  0001 C CNN
F 3 "~" H 2400 4600 50  0001 C CNN
	1    2400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4400 2100 4400
Wire Wire Line
	2600 4400 2600 4000
$Comp
L Device:D D7
U 1 1 6155A8CA
P 1950 3900
F 0 "D7" H 1950 4117 50  0000 C CNN
F 1 "D" H 1950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 1950 3900 50  0001 C CNN
F 3 "~" H 1950 3900 50  0001 C CNN
	1    1950 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 6155A8D0
P 2400 3900
F 0 "SW7" H 2400 4185 50  0000 C CNN
F 1 "SW_Push" H 2400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 2400 4100 50  0001 C CNN
F 3 "~" H 2400 4100 50  0001 C CNN
	1    2400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3900 2100 3900
Wire Wire Line
	2600 3900 2600 3500
$Comp
L Device:D D40
U 1 1 6155A8D8
P 8950 4900
F 0 "D40" H 8950 5117 50  0000 C CNN
F 1 "D" H 8950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 8950 4900 50  0001 C CNN
F 3 "~" H 8950 4900 50  0001 C CNN
	1    8950 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW40
U 1 1 6155A8DE
P 9400 4900
F 0 "SW40" H 9400 5185 50  0000 C CNN
F 1 "SW_Push" H 9400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 9400 5100 50  0001 C CNN
F 3 "~" H 9400 5100 50  0001 C CNN
	1    9400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4900 9100 4900
Wire Wire Line
	9600 4900 9600 4500
$Comp
L Device:D D10
U 1 1 6155A8E6
P 1950 5400
F 0 "D10" H 1950 5617 50  0000 C CNN
F 1 "D" H 1950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 1950 5400 50  0001 C CNN
F 3 "~" H 1950 5400 50  0001 C CNN
	1    1950 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 6155A8EC
P 2400 5400
F 0 "SW10" H 2400 5685 50  0000 C CNN
F 1 "SW_Push" H 2400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 2400 5600 50  0001 C CNN
F 3 "~" H 2400 5600 50  0001 C CNN
	1    2400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5400 2100 5400
Wire Wire Line
	2600 5400 2600 5000
$Comp
L Device:D D20
U 1 1 6155A8F4
P 3950 5900
F 0 "D20" H 3950 6117 50  0000 C CNN
F 1 "D" H 3950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 3950 5900 50  0001 C CNN
F 3 "~" H 3950 5900 50  0001 C CNN
	1    3950 5900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW20
U 1 1 6155A8FA
P 4400 5900
F 0 "SW20" H 4400 6185 50  0000 C CNN
F 1 "SW_Push" H 4400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 4400 6100 50  0001 C CNN
F 3 "~" H 4400 6100 50  0001 C CNN
	1    4400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5900 4100 5900
Wire Wire Line
	4600 5900 4600 5500
Wire Wire Line
	1800 3900 1800 4400
Connection ~ 1800 4400
Connection ~ 1800 5400
Text GLabel 7800 6000 3    50   Input ~ 0
SW_Key_In_8
$Comp
L Device:D D4
U 1 1 6155A90C
P 950 4400
F 0 "D4" H 950 4617 50  0000 C CNN
F 1 "D" H 950 4526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 950 4400 50  0001 C CNN
F 3 "~" H 950 4400 50  0001 C CNN
	1    950  4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 6155A912
P 1400 4400
F 0 "SW4" H 1400 4685 50  0000 C CNN
F 1 "SW_Push" H 1400 4594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 1400 4600 50  0001 C CNN
F 3 "~" H 1400 4600 50  0001 C CNN
	1    1400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 4400 1100 4400
Wire Wire Line
	1600 4400 1600 4000
$Comp
L Device:D D3
U 1 1 6155A91A
P 950 3900
F 0 "D3" H 950 4117 50  0000 C CNN
F 1 "D" H 950 4026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 950 3900 50  0001 C CNN
F 3 "~" H 950 3900 50  0001 C CNN
	1    950  3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 6155A920
P 1400 3900
F 0 "SW3" H 1400 4185 50  0000 C CNN
F 1 "SW_Push" H 1400 4094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 1400 4100 50  0001 C CNN
F 3 "~" H 1400 4100 50  0001 C CNN
	1    1400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3900 1100 3900
Wire Wire Line
	1600 3900 1600 3500
$Comp
L Device:D D5
U 1 1 6155A928
P 950 4900
F 0 "D5" H 950 5117 50  0000 C CNN
F 1 "D" H 950 5026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 950 4900 50  0001 C CNN
F 3 "~" H 950 4900 50  0001 C CNN
	1    950  4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 6155A92E
P 1400 4900
F 0 "SW5" H 1400 5185 50  0000 C CNN
F 1 "SW_Push" H 1400 5094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 1400 5100 50  0001 C CNN
F 3 "~" H 1400 5100 50  0001 C CNN
	1    1400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 4900 1100 4900
Wire Wire Line
	1600 4900 1600 4500
$Comp
L Device:D D6
U 1 1 6155A936
P 950 5400
F 0 "D6" H 950 5617 50  0000 C CNN
F 1 "D" H 950 5526 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 950 5400 50  0001 C CNN
F 3 "~" H 950 5400 50  0001 C CNN
	1    950  5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 6155A93C
P 1400 5400
F 0 "SW6" H 1400 5685 50  0000 C CNN
F 1 "SW_Push" H 1400 5594 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 1400 5600 50  0001 C CNN
F 3 "~" H 1400 5600 50  0001 C CNN
	1    1400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5400 1100 5400
Wire Wire Line
	1600 5400 1600 5000
$Comp
L Device:D D11
U 1 1 6155A944
P 1950 5900
F 0 "D11" H 1950 6117 50  0000 C CNN
F 1 "D" H 1950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 1950 5900 50  0001 C CNN
F 3 "~" H 1950 5900 50  0001 C CNN
	1    1950 5900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 6155A94A
P 2400 5900
F 0 "SW11" H 2400 6185 50  0000 C CNN
F 1 "SW_Push" H 2400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 2400 6100 50  0001 C CNN
F 3 "~" H 2400 6100 50  0001 C CNN
	1    2400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5900 2100 5900
Wire Wire Line
	2600 5900 2600 5500
Wire Wire Line
	800  3900 800  4400
Connection ~ 800  4400
Wire Wire Line
	800  5400 800  4900
Connection ~ 800  5400
Text GLabel 8800 6000 3    50   Input ~ 0
SW_Key_In_9
Wire Wire Line
	9700 3500 9600 3500
Wire Wire Line
	9600 3500 8600 3500
Connection ~ 9600 3500
Wire Wire Line
	8600 3500 7600 3500
Connection ~ 8600 3500
Wire Wire Line
	7600 3500 6600 3500
Connection ~ 7600 3500
Wire Wire Line
	6600 3500 5600 3500
Connection ~ 6600 3500
Wire Wire Line
	4600 3500 5600 3500
Connection ~ 5600 3500
Wire Wire Line
	4600 3500 3600 3500
Connection ~ 4600 3500
Wire Wire Line
	3600 3500 2600 3500
Connection ~ 3600 3500
Wire Wire Line
	2600 3500 1600 3500
Connection ~ 2600 3500
Wire Wire Line
	9700 4000 9600 4000
Wire Wire Line
	9600 4000 8600 4000
Wire Wire Line
	8600 4000 8600 4400
Connection ~ 9600 4000
Wire Wire Line
	8600 4000 7600 4000
Connection ~ 8600 4000
Wire Wire Line
	7600 4000 6600 4000
Connection ~ 7600 4000
Wire Wire Line
	6600 4000 5600 4000
Connection ~ 6600 4000
Wire Wire Line
	5600 4000 4600 4000
Connection ~ 5600 4000
Wire Wire Line
	4600 4000 3600 4000
Connection ~ 4600 4000
Wire Wire Line
	3600 4000 2600 4000
Connection ~ 3600 4000
Wire Wire Line
	2600 4000 1600 4000
Connection ~ 2600 4000
Connection ~ 4800 4900
Wire Wire Line
	4800 4400 4800 4900
Connection ~ 3800 4900
Wire Wire Line
	3800 4400 3800 4900
Connection ~ 800  4900
Wire Wire Line
	800  4400 800  4900
Wire Wire Line
	6600 4500 5600 4500
Connection ~ 6600 4500
Wire Wire Line
	5600 4500 4600 4500
Connection ~ 5600 4500
Wire Wire Line
	4600 4500 3600 4500
Connection ~ 4600 4500
Connection ~ 3600 4500
Wire Wire Line
	6600 5000 5600 5000
Connection ~ 6600 5000
Wire Wire Line
	5600 5000 4600 5000
Connection ~ 5600 5000
Connection ~ 4600 5000
Wire Wire Line
	2600 5000 1600 5000
Connection ~ 2600 5000
Text GLabel 9300 2500 0    50   Input ~ 0
SW_Key_Out_1
Text GLabel 10900 2200 2    50   Input ~ 0
SW_Key_Out_2
Text GLabel 10900 2100 2    50   Input ~ 0
SW_Key_Out_3
Text GLabel 10900 1800 2    50   Input ~ 0
SW_Key_Out_4
Text GLabel 10900 1600 2    50   Input ~ 0
SW_Key_Out_5
Text GLabel 9300 1300 0    50   Input ~ 0
SW_Key_In_1
Text GLabel 9300 1400 0    50   Input ~ 0
SW_Key_In_2
Text GLabel 9300 1500 0    50   Input ~ 0
SW_Key_In_3
Text GLabel 9300 1600 0    50   Input ~ 0
SW_Key_In_4
Text GLabel 9300 1700 0    50   Input ~ 0
SW_Key_In_5
Text GLabel 9300 1800 0    50   Input ~ 0
SW_Key_In_6
Text GLabel 9300 2200 0    50   Input ~ 0
SW_Key_In_7
Text GLabel 10900 1900 2    50   Input ~ 0
SW_Key_In_8
Text GLabel 10900 2300 2    50   Input ~ 0
SW_Key_In_9
Wire Wire Line
	8800 4400 8800 4900
Wire Wire Line
	9600 5900 9600 5500
Wire Wire Line
	9200 5900 9100 5900
$Comp
L Switch:SW_Push SW42
U 1 1 61427943
P 9400 5900
F 0 "SW42" H 9400 6185 50  0000 C CNN
F 1 "SW_Push" H 9400 6094 50  0000 C CNN
F 2 "Keyswitch:Kailh_socket_MX" H 9400 6100 50  0001 C CNN
F 3 "~" H 9400 6100 50  0001 C CNN
	1    9400 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:D D42
U 1 1 6142793D
P 8950 5900
F 0 "D42" H 8950 6117 50  0000 C CNN
F 1 "D" H 8950 6026 50  0000 C CNN
F 2 "Ble_keyboard:D_LL4148" H 8950 5900 50  0001 C CNN
F 3 "~" H 8950 5900 50  0001 C CNN
	1    8950 5900
	1    0    0    -1  
$EndComp
Connection ~ 8800 5900
Wire Wire Line
	8800 5900 8800 6000
Connection ~ 9600 5500
Wire Wire Line
	9600 5500 9700 5500
Wire Wire Line
	5800 5400 5800 5900
Wire Wire Line
	3800 5400 3800 5900
Connection ~ 8800 5400
Wire Wire Line
	8800 5400 8800 5900
Connection ~ 9600 5000
Wire Wire Line
	9600 5000 9700 5000
Wire Wire Line
	2800 4900 2800 5400
Wire Wire Line
	2600 5000 3600 5000
Connection ~ 8800 4900
Wire Wire Line
	8800 4900 8800 5400
Connection ~ 9600 4500
Wire Wire Line
	9600 4500 9700 4500
Wire Wire Line
	1800 4400 1800 4900
Wire Wire Line
	1600 4500 2600 4500
Text GLabel 10900 1200 2    50   Input ~ 0
SW_setting
$Comp
L power:GND #PWR01
U 1 1 61C861AE
P 11650 1750
F 0 "#PWR01" H 11650 1500 50  0001 C CNN
F 1 "GND" H 11655 1577 50  0000 C CNN
F 2 "" H 11650 1750 50  0001 C CNN
F 3 "" H 11650 1750 50  0001 C CNN
	1    11650 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	11650 1700 11650 1750
$Comp
L power:GND #PWR01
U 1 1 61C95815
P 11650 1150
F 0 "#PWR01" H 11650 900 50  0001 C CNN
F 1 "GND" H 11655 977 50  0000 C CNN
F 2 "" H 11650 1150 50  0001 C CNN
F 3 "" H 11650 1150 50  0001 C CNN
	1    11650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11650 1100 11650 1150
Wire Wire Line
	11650 1100 10900 1100
Wire Wire Line
	11650 1700 10900 1700
NoConn ~ 9300 1200
NoConn ~ 9300 2300
NoConn ~ 9300 2600
NoConn ~ 9300 2700
NoConn ~ 9300 2800
NoConn ~ 10900 1300
NoConn ~ 10900 1400
NoConn ~ 10900 1500
NoConn ~ 10900 2000
NoConn ~ 10900 2400
NoConn ~ 10900 2500
NoConn ~ 10900 2600
NoConn ~ 10900 2700
NoConn ~ 10900 2800
NoConn ~ 10900 2900
Wire Wire Line
	1500 1000 2500 1000
$Comp
L Ble_Keyboard:MCP73831T-2ACI_OT U1
U 1 1 61366EB5
P 3200 1900
F 0 "U1" H 3200 2370 50  0000 C CNN
F 1 "MCP73831T-2ACI_OT" H 3200 2279 50  0000 C CNN
F 2 "Ble_keyboard:SOT95P280X145-5N" H 3200 1900 50  0001 L BNN
F 3 "56K7045" H 3200 1900 50  0001 L BNN
F 4 "1332158" H 3200 1900 50  0001 L BNN "Field4"
F 5 "MCP73831T-2ACI/OT" H 3200 1900 50  0001 L BNN "Field5"
F 6 "Microchip" H 3200 1900 50  0001 L BNN "Field6"
F 7 "SOT-5" H 3200 1900 50  0001 L BNN "Field7"
	1    3200 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 1800 2500 1000
Connection ~ 2500 1000
Wire Wire Line
	2500 1000 4500 1000
Wire Wire Line
	9300 2400 8650 2400
$Comp
L Switch:SW_SPDT SW1
U 1 1 61EC7190
P 6800 2000
F 0 "SW1" H 6800 2285 50  0000 C CNN
F 1 "SW_SPDT" H 6800 2194 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPDT_PCM12" H 6800 2000 50  0001 C CNN
F 3 "~" H 6800 2000 50  0001 C CNN
	1    6800 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5400 4800 5900
Connection ~ 2800 5400
Connection ~ 3600 5000
Wire Wire Line
	3600 5000 4600 5000
Wire Wire Line
	6600 5000 7600 5000
Connection ~ 1800 4900
Wire Wire Line
	1800 4900 1800 5400
Connection ~ 2600 4500
Wire Wire Line
	2600 4500 3600 4500
Connection ~ 4800 5900
Wire Wire Line
	4800 5900 4800 6000
Connection ~ 5600 5500
Wire Wire Line
	5600 5500 6600 5500
Wire Wire Line
	2800 5400 2800 6000
Connection ~ 3800 5900
Wire Wire Line
	3800 5900 3800 6000
Connection ~ 4600 5500
Wire Wire Line
	4600 5500 5600 5500
Wire Wire Line
	1800 5400 1800 5900
Connection ~ 1800 5900
Wire Wire Line
	1800 5900 1800 6000
Wire Wire Line
	2600 5500 4600 5500
Wire Wire Line
	800  5400 800  6000
Wire Wire Line
	6600 4500 7600 4500
Connection ~ 5800 5900
Wire Wire Line
	5800 5900 5800 6000
Connection ~ 6600 5500
Wire Wire Line
	6800 4400 6800 4900
Wire Wire Line
	6600 5500 7600 5500
Connection ~ 6800 5900
Wire Wire Line
	6800 5900 6800 6000
Connection ~ 7600 5500
Wire Wire Line
	7600 5500 9600 5500
Connection ~ 6800 5400
Wire Wire Line
	6800 5400 6800 5900
Connection ~ 7600 5000
Wire Wire Line
	7600 5000 9600 5000
Connection ~ 6800 4900
Wire Wire Line
	6800 4900 6800 5400
Connection ~ 7600 4500
Wire Wire Line
	7800 4400 7800 6000
Wire Wire Line
	7600 4500 9600 4500
$Comp
L power:GND #PWR01
U 1 1 616B4E45
P 11000 5500
F 0 "#PWR01" H 11000 5250 50  0001 C CNN
F 1 "GND" H 11005 5327 50  0000 C CNN
F 2 "" H 11000 5500 50  0001 C CNN
F 3 "" H 11000 5500 50  0001 C CNN
	1    11000 5500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
